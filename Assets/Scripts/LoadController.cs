﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using RenderHeads.Media.AVProVideo;
using SFB;
using System.IO;

public class LoadController : MonoBehaviour {

    MediaPlayer mp;
    public GameObject player;
    public DisplayIMGUI dIMGUI;
    int i = 0;
    bool events = false;
    bool instance = false;
    private string _path;
    private ExtensionFilter[] filters = new ExtensionFilter[2];

    void Start()
    {
        filters[0] = new ExtensionFilter("Video Files", "mp4", "avi", "flv");
        filters[1] = new ExtensionFilter("More Video Files", "mp4", "avi", "flv");
    }

    public void openFile()
    {
        if (Application.platform == RuntimePlatform.WindowsPlayer || Application.platform == RuntimePlatform.OSXPlayer)
        {
            saveResult(StandaloneFileBrowser.OpenFilePanel("Open File", "", filters, false));

        }
    }

    private void saveResult(string[] paths)
    {
        if (paths.Length == 0)
        {
            return;
        }

        _path = "";
        foreach (var p in paths)
        {
            _path = Path.Combine(_path, p);
        }
    }

    public void LoadVideo()
    {
        //mp = player.GetComponent<MediaPlayer>();
        //if (Application.platform == RuntimePlatform.WindowsPlayer || Application.platform == RuntimePlatform.OSXPlayer)
        //{
            saveResult(StandaloneFileBrowser.OpenFilePanel("Open File", "", filters, false));

        //}
        MediaPlayer test = AddEvents(events);
        if (test == null) mp = test;
        //mp.Events.AddListener(VideoEvent);
        mp.m_AutoStart = false;
        //mp = GameObject.Find("VideoPlayer").GetComponent<MediaPlayer>();
        string path = "AVProVideoSamples/BigBuckBunny_360p30.mp4";
        string[] paths = { "AVProVideoSamples/BigBuckBunny_360p30.mp4", "AVProVideoSamples/pepsi_commercial.mp4" };
        //AVProVideoSamples/pepsi_commercial.mp4
        //AVProVideoSamples/BigBuckBunny_360p30.mp4
        foreach(string p in paths)
        {
            mp.m_VideoPath = p;
            Debug.Log(p);
            mp.OpenVideoFromFile(MediaPlayer.FileLocation.RelativeToStreamingAssetsFolder, p, true);
        }
        
        //mp.Play();
        //dIMGUI.
    }

    public void VideoEvent(MediaPlayer mp, MediaPlayerEvent.EventType et, ErrorCode err)
    {
        switch (et)
        {
            case MediaPlayerEvent.EventType.ReadyToPlay:
                mp.Control.Play();
                break;
            case MediaPlayerEvent.EventType.FirstFrameReady:
                Debug.Log("First frame ready");
                break;
            case MediaPlayerEvent.EventType.FinishedPlaying:
                mp.m_VideoPath = "";
                mp.CloseVideo();
                i++;
                break;
        }
        Debug.Log("Event: " + et.ToString());
    }

    private MediaPlayer AddEvents(bool events)
    {
        if (!events)
        {
            this.mp = this.player.GetComponent<MediaPlayer>();
            this.mp.Events.AddListener(VideoEvent);
            events = true;
            return this.mp;
        }
        return null;
    }

}
